#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec4 vertexPosition_modelspace;
layout(location = 1) in vec3 normal_modelspace;

uniform mat4 modelView_Matrix;
uniform mat4 modelViewProjection_Matrix;
uniform mat4 normal_Matrix;

uniform vec3 lightPosition_eyeCoord;

const float kd = 0.7;


out vec3 vColor;

void main()
{
	vec3 vertexPosition_eyespace = (vertexPosition_modelspace * modelView_Matrix).xyz;
	gl_Position = vertexPosition_modelspace * modelViewProjection_Matrix;
	
    vec3 normal_eyespace = (vec4(normal_modelspace,0.0) * normal_Matrix).xyz;
    vec3 lightDir = normalize(lightPosition_eyeCoord - vertexPosition_eyespace);
    float difuse = kd * max(
    			dot( lightDir , normal_eyespace ),
    			0.0f);
   	//vColor =  normal_eyespace.xyz;
   	//vColor = vec3(1,0,0);
    vColor = vec3(1,1,1) * (difuse + 0.3);
    			
    
    //vColor = vertexPosition_modelspace.xyz;
    //vColor = normalPosition_modelspace.xyz;
}
