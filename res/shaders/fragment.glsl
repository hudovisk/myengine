#version 330 core

// Ouput data
in vec3 vColor;
out vec4 fragColor;


void main()
{
   fragColor.xyz = vColor;
}
