
#include "gtest/gtest.h"
#include "Transform.h"
#include "Vec4.h"

#include <iostream>
class TransformTest : public ::testing::Test
{
public:

};

const float PIdiv180 = 0.01745329251;

TEST_F(TransformTest, initializationIdentity)
{
	bool passed = true;

	Transform transform;

	Matrix4 m1 = transform.getMatrix();

	for(int i=0; i<4; i++)
	{
		for(int j=0; j<4; j++)
		{
			if(i != j && m1.m_data[i*4 + j] != 0)
			{
				passed = false;
			}
		}
		if(m1.m_data[i*4 + i] != 1)
		{
			passed = false;
		}
	}

	ASSERT_EQ(passed,true);
}

TEST_F(TransformTest, translate)
{
	Transform transform;

	transform.translate(Vec3(100,50,-35));

	Matrix4 m1 = transform.getMatrix();

	Matrix4 correct;
	correct.m_data[12] = 100;
	correct.m_data[13] = 50;
	correct.m_data[14] = -35;

	for(int i = 0; i < 16; i++)
	{
		ASSERT_NEAR(m1.m_data[i], correct.m_data[i],0.00001);
	}
}

TEST_F(TransformTest, rotate)
{
	Transform transform;

	Vec3 axis(1,1,0);
	axis = axis.normalize();
	float angle = 90;

	transform.setRotationInDegree(angle, axis);

	Matrix4 m1 = transform.getMatrix();
	Matrix4 m2;
	float *correct = m2.m_data;
	float x = axis.m_data[0];
	float y = axis.m_data[1];
	float z = axis.m_data[2];
	float angleInRadian = angle*PIdiv180;

	correct[0] = cos(angleInRadian) + x*x*(1 - cos(angleInRadian)); 	correct[1] = x*y*(1 - cos(angleInRadian)) - z*sin(angleInRadian); 	correct[2] = x*z*(1 - cos(angleInRadian)) + y*sin(angleInRadian);
	correct[4] = y*x*(1-cos(angleInRadian)) + z*sin(angleInRadian); 	correct[5] = cos(angleInRadian) + y*y*(1 - cos(angleInRadian)); 	correct[6] = y*z*(1 - cos(angleInRadian)) - x*sin(angleInRadian);
	correct[8] = z*x*(1 - cos(angleInRadian)) -y*sin(angleInRadian); 	correct[9] = z*y*(1 - cos(angleInRadian)) + x*sin(angleInRadian);	correct[10] = cos(angleInRadian) + z*z*(1 - cos(angleInRadian));

	for(int i=0; i<16; i++)
	{

		ASSERT_NEAR(m1.m_data[i],m2.m_data[i],0.0001);
	}
	axis = Vec3(0,1,1);
	angle = 23.23;
	transform.rotateInDegree(angle,axis.normalize());

	x = axis.m_data[0];
	y = axis.m_data[1];
	z = axis.m_data[2];
	angleInRadian = (angle)*PIdiv180;

	m1 = transform.getMatrix();
	Matrix4 m3;
	correct = m3.m_data;

	//wikipedia: rotation matrix;
	correct[0] = cos(angleInRadian) + x*x*(1 - cos(angleInRadian)); 	correct[1] = x*y*(1 - cos(angleInRadian)) - z*sin(angleInRadian); 	correct[2] = x*z*(1 - cos(angleInRadian)) + y*sin(angleInRadian);
	correct[4] = y*x*(1-cos(angleInRadian)) + z*sin(angleInRadian); 	correct[5] = cos(angleInRadian) + y*y*(1 - cos(angleInRadian)); 	correct[6] = y*z*(1 - cos(angleInRadian)) - x*sin(angleInRadian);
	correct[8] = z*x*(1 - cos(angleInRadian)) -y*sin(angleInRadian); 	correct[9] = z*y*(1 - cos(angleInRadian)) + x*sin(angleInRadian);	correct[10] = cos(angleInRadian) + z*z*(1 - cos(angleInRadian));

	m2 = m2 * m3;

	for(int i=0; i<16; i++)
	{

		ASSERT_NEAR(m1.m_data[i],m2.m_data[i],0.0001);
	}

}

TEST_F(TransformTest, scale)
{
	Transform transform;
	Vec3 scale(10,2.5,-4);

	transform.scale(scale);

	Matrix4 m1 = transform.getMatrix();
	Matrix4 m2;

	m2.m_data[0] = scale.m_data[0];
	m2.m_data[5] = scale.m_data[1];
	m2.m_data[10] = scale.m_data[2];

	for(int i=0; i<4; i++)
	{
		ASSERT_NEAR(m1.m_data[i], m2.m_data[i], 0.0001);
	}
}

TEST_F(TransformTest, rotateScaleTranslateOriginPoint)
{
	Vec4 ponto(0,0,0,1);

	Transform transform;

	transform.translate(Vec3(10,10,10));
	transform.scale(Vec3(5,5,5));
	transform.setRotationInDegree(90,Vec3(1,0,0));

	Vec4 novoPonto = ponto * transform.getMatrix();

	ASSERT_NEAR(novoPonto.m_data[0],10,0.00001);
	ASSERT_NEAR(novoPonto.m_data[1],10,0.00001);
	ASSERT_NEAR(novoPonto.m_data[2],10,0.00001);

	Vec4 inversePonto = novoPonto * transform.getInverseMatrix();

	ASSERT_NEAR(inversePonto.m_data[0],0,0.00001);
	ASSERT_NEAR(inversePonto.m_data[1],0,0.00001);
	ASSERT_NEAR(inversePonto.m_data[2],0,0.00001);
}

TEST_F(TransformTest, rotateScaleTranslateNotOriginPoint)
{
	Vec4 ponto(5, 3.2, -2, 1);

	Transform transform;

	transform.translate(Vec3(10, 10, 10));
	transform.scale(Vec3(5, 5, 5));
	transform.setRotationInDegree(180, Vec3(0,1,0));

	Vec4 novoPonto = ponto * transform.getMatrix();

	ASSERT_NEAR(novoPonto.m_data[0], -15, 0.00001);
	ASSERT_NEAR(novoPonto.m_data[1], 26, 0.00001);
	ASSERT_NEAR(novoPonto.m_data[2], 20, 0.00001); 

	Vec4 inversePonto = novoPonto * transform.getInverseMatrix();

	ASSERT_NEAR(inversePonto.m_data[0], 5, 0.00001);
	ASSERT_NEAR(inversePonto.m_data[1], 3.2, 0.00001);
	ASSERT_NEAR(inversePonto.m_data[2], -2, 0.00001);
}


