
#pragma once

#include "InputTypes.h"
#include "IEventManager.h"

#include <list>
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

class MenuDebugger
{
public:
	MenuDebugger();
	~MenuDebugger() { }

	bool init();
	bool destroy();

	void render();
	void update(float updateTime, std::list<InputEvent>& inputs);

	void onWindowResize(IEventDataPtr event);
	void onEnginePause(IEventDataPtr event);
	void onEngineResume(IEventDataPtr event);

private:
	void translateKeyboardInput(CEGUI::GUIContext& context, 
				const InputEvent& input);
	void transalteMouseInput(CEGUI::GUIContext& context, 
				const InputEvent& input);

	CEGUI::MouseButton toCEGUIMouseButton(MouseCode button);
	CEGUI::Key::Scan toCEGUIKey(SDL_Keycode key);

	CEGUI::OpenGL3Renderer& m_renderer;
	bool m_enabled;	
};