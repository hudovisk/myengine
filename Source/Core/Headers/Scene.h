#pragma once

#include <vector>
#include "GameObject.h"

class Scene
{
public:
	std::vector<GameObject> objects;
	std::vector<Light> lights;

	Camera camera;
};
