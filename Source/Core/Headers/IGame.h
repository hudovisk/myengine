
#pragma once

#include <list>

#include "InputTypes.h"

class IGame
{
public:
  
	virtual ~IGame() { }

	virtual bool init() = 0;
	virtual bool destroy() = 0;
  
	virtual void update(float updateTime, std::list<InputEvent>& inputs) = 0;
  
	virtual void render() = 0;
};
