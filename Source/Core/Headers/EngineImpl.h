/*
 * EngineImpl.h
 *
 *  Created on: Apr 4, 2014
 *      Author: hudo
 */

#pragma once

#include "IEngine.h"

#include "IGame.h"
#include "EventManagerImpl.h"
#include "OpenGLRenderManager.h"
#include "MenuDebugger.h"
#include "InputContext.h"

class EngineImpl : public IEngine
{
public:
	EngineImpl(IGame* game);
	~EngineImpl();

	bool init();
	bool destroy();

	void mainLoop();
	
	void update(float updateTime);
	void render();

	void onWindowClosed(IEventDataPtr event);
private:
	bool m_bInitialised;
	Engine::EngineState m_state;

	IGame *m_game;
	MenuDebugger *m_menuDebugger;
	InputContext m_inputContext;
};
