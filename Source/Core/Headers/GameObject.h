/*
 * GameObject.h
 *
 *  Created on: Apr 13, 2014
 *      Author: hudo
 */

#pragma once

#include "IRenderComponent.h"
#include "IController.h"
#include <Transform.h>
#include <memory>
#include "MeshRender.h"
#include "Vec4.h"

const float PIdiv180 = 0.017453;

class Scene;

class GameObject
{
public:
	GameObject() { }

	virtual ~GameObject() { destroy(); }
	
	virtual bool init() { return true; }

	virtual bool destroy() { return true; }
	
	virtual void setMeshRender(std::shared_ptr<MeshRender> meshRender) { m_meshRender = meshRender; }
	virtual std::shared_ptr<MeshRender> getMeshRender() { return m_meshRender; }

	virtual void setController(std::shared_ptr<IController> controller) { m_controller = controller; }
	virtual std::shared_ptr<IController> getController() { return m_controller; }

	virtual std::shared_ptr<Transform> getTransform() { return m_transform; }
	virtual void setTransform(std::shared_ptr<Transform> transform) { m_transform = transform;}
protected:
	std::shared_ptr<MeshRender> m_meshRender;
	std::shared_ptr<IController> m_controller;
	std::shared_ptr<Transform> m_transform;
};

class Camera : public GameObject
{
public:
	Camera() :
		m_fov(90 * PIdiv180), m_nearDistance(1), m_farDistance(10),
		m_aspectRatio(1), m_dirty(true), m_projectionMatrix(Matrix4())
	{
	}

	Camera(float fov, float nearDistance, float farDistance)
		: m_fov(fov), m_nearDistance(nearDistance), m_farDistance(farDistance),
		  m_aspectRatio(1), m_dirty(true), m_projectionMatrix(Matrix4())
	{
	}

	virtual ~Camera() { destroy(); }

	Matrix4& getProjectionMatrix();

	Matrix4& getProjectionMatrix(float fov, float aspectRatio, float nearDistance, float farDistance);

	void setFov(float fov) { m_dirty = true; m_fov = fov * PIdiv180; }
	void setNearDistance(float nearDistance) { m_dirty = true; m_nearDistance = nearDistance; }
	void setFarDistance(float farDistance) { m_dirty = true; m_farDistance = farDistance; }
	void setAspectRatio(float aspectRatio) { m_dirty = true; m_aspectRatio = aspectRatio; }
	float getFov() { return m_fov; }
	float getNearDistance() { return m_nearDistance; }
	float getFarDistance() { return m_farDistance; }

private:
	float m_fov;
	float m_nearDistance;
	float m_farDistance;
	float m_aspectRatio;
	bool m_dirty;
	Matrix4 m_projectionMatrix;
};

//typedef enum{
//	LIGHT_DIRECTIONAL,
//	LIGHT_POINT,
//} LightType;

class Light : public GameObject
{
public:
	Light(float intensity)
		: m_intensity(intensity)
	{ }

	~Light()
	{ }

	void setIntensity(float intensity) { m_intensity = intensity; }
	float getIntensity() { return m_intensity; }

	Vec3 getPos()
	{
		Vec4 result = Vec4(0,0,0,1) * m_transform->getMatrix();
		return Vec3(result.m_data[0], result.m_data[1], result.m_data[2]);
	}

private:
	float m_intensity;
};
