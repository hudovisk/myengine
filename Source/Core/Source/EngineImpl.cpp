/*
 * Engine.cpp
 *
 *  Created on: Apr 3, 2014
 *      Author: hudo
 */

#include <chrono>
#include <thread>

#include "EngineImpl.h"
#include "MyAssert.h"
#include "Logger.h"

enum {
	PAUSED_RESUMED_MSG = 0x01,
	EXITED_MSG = 0x02
};

IEventManager *g_eventManager = nullptr;
OpenGLRenderManager *g_renderManager = nullptr;

EngineImpl::EngineImpl(IGame* game) :
	m_bInitialised(false), m_state(Engine::NOT_STARTED), m_game(game),
	m_menuDebugger(nullptr)
{
}

EngineImpl::~EngineImpl()
{
	destroy();
}

bool EngineImpl::destroy()
{  
	m_game->destroy();
  
	g_eventManager->destroy();

	g_renderManager->destroy();
	
	Logger::destroy();

	return true;
}

bool EngineImpl::init()
{
	Logger::init("log.txt",ALL);

	g_eventManager = new EventManagerImpl();
	if(nullptr == g_eventManager || !g_eventManager->init())
	{
		ASSERT(false,"Event Manager initialization error");
	}
	LOG(INFO, "Event Manager initialized");

	g_renderManager = new OpenGLRenderManager();
	if(nullptr == g_renderManager || !g_renderManager->init(800,800))
	{
		ASSERT(false,"Render Manager initialization error");
	}
	LOG(INFO, "Render Manager initialized");

	if(!m_game->init())
	{
		ASSERT(false, "Game initialization error");
	}
	LOG(INFO, "Game initialized");

	m_menuDebugger = new MenuDebugger();
	if(!m_menuDebugger->init())
	{
		ASSERT(false, "Menu debugger initialization error");
	}
	LOG(INFO, "Menu debugger initialized");

	if(!m_inputContext.init("res/mappings/engineInputMap.xml"))
	{
		ASSERT(false, "Engine input context initialization error");
	}

	m_bInitialised = true;

	g_eventManager->addListenner(EventListenerDelegate::from_method<EngineImpl,&EngineImpl::onWindowClosed>(this),(new WindowClosedEventData)->getType());

	return m_bInitialised;
}

void EngineImpl::mainLoop()
{
	using std::chrono::high_resolution_clock;
	using duration = high_resolution_clock::duration;

	std::chrono::milliseconds FRAME_DURATION(16);

	m_state = Engine::RUNNING;
	auto last = high_resolution_clock::now();
	double updateTime = 0;
	
	unsigned int frames = 0;
	auto frameTimer = last;
	while(m_state == Engine::RUNNING || m_state == Engine::PAUSED)
	{	  
		update(updateTime);
		
		g_eventManager->dispatchEvents();
		
		render();
		frames++;
			
		auto now = high_resolution_clock::now();
		updateTime = static_cast<double>( (now - last).count() * duration::period::num ) / duration::period::den; //conversão da diferença para segundos.
		

//		std::cout<<"updateTime: "<<updateTime<<" FRAME_TIME: "<<FRAME_TIME<<std::endl;

		if(FRAME_DURATION > (now - last))
		{
			std::this_thread::sleep_for(FRAME_DURATION - (now - last));
		}

		float frameSeconds = (now - frameTimer).count() / duration::period::den;
		if(frameSeconds >= 1)
		{
		    // std::cout<<"FPS: "<<(float) frames / frameSeconds <<std::endl;
		    frames = 0;
		    frameTimer = now;
		}

		last = now;
	}
}

void EngineImpl::update(float updateTime)
{
	std::list<InputEvent>& inputs = g_eventManager->getInputQueue();
	std::vector<InputMessage> inputMessages = m_inputContext.parse(inputs);
	for(auto itMessage = inputMessages.begin(); itMessage != inputMessages.end();
					itMessage++)
	{
		switch(itMessage->id)
		{
			case PAUSED_RESUMED_MSG:
				if(m_state == Engine::PAUSED)
				{
					m_state = Engine::RUNNING;
					g_eventManager->triggerEvent(IEventDataPtr(new EngineResumedEventData()));
				}
				else
				{
					m_state = Engine::PAUSED;
					g_eventManager->triggerEvent(IEventDataPtr(new EnginePausedEventData()));	
				}
			break;
			case EXITED_MSG:
				m_state = Engine::EXITED;
			break;
		}
	}

	m_menuDebugger->update(updateTime,g_eventManager->getInputQueue());
	
	if(m_state == Engine::RUNNING)
		m_game->update(updateTime,g_eventManager->getInputQueue());

	g_eventManager->getInputQueue().clear();
}

void EngineImpl::render()
{
	g_renderManager->preRender();

	m_game->render();
	m_menuDebugger->render();

	g_renderManager->postRender();
}

void EngineImpl::onWindowClosed(IEventDataPtr event)
{
	m_state = Engine::EXITED;
}



