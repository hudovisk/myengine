/*
 * InputContext.h
 *
 *  Created on: Mar 31, 2014
 *      Author: hudo
 */

#pragma once

#include <string>
#include <map>
#include <vector>
#include <list>
#include "InputTypes.h"
#include "rapidxml-1.13/rapidxml.hpp"

class InputMessage
{
public:
	unsigned int id;
	union
	{
		float value;
		float pos[2];
	};
};

class InputContext
{
public:
	InputContext();

	bool init(std::string filename);

	std::vector<InputMessage> parse(std::list<InputEvent>& input);
private:

	class MouseMapAttributes
	{
	public:
		unsigned int m_mouseCode;
		unsigned int m_messageId;
		bool m_invertedX;
		bool m_relativeX;
		bool m_relativeY;
		bool m_invertedY;
		bool m_pressed;
	  
	};

	using MouseMap = std::pair<unsigned int, std::vector<MouseMapAttributes>>;
	using KeyMap = std::pair<KeyboardEvent, std::vector<unsigned int>>;
	
	KeyboardEvent mapXmlKeyNode(rapidxml::xml_node<>* node);
	MouseMapAttributes mapXmlMouseNode(rapidxml::xml_node<>* node);
	
	void setInvertedAxis(MouseMapAttributes& mouseAttribute, rapidxml::xml_node<>* node);

	std::map<KeyboardEvent, std::vector<unsigned int>> m_keyMap;
	std::map<unsigned int, std::vector<MouseMapAttributes>> m_mouseMap;
	//joystick map
};

