/*
 * OpenGLRenderManager.cpp
 *
 *  Created on: Apr 12, 2014
 *      Author: hudo
 */

#include "OpenGLRenderManager.h"
#include "MyAssert.h"
#include "Logger.h"

extern IEventManager* g_eventManager;

bool OpenGLRenderManager::init(int width, int height)
{
	m_width = width;
	m_height = height;

	m_window = SDL_CreateWindow( "My Engine", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_width, m_height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	//Get window surface
	m_screenSurface = SDL_GetWindowSurface( m_window );
	//Fill the surface white
	SDL_FillRect( m_screenSurface, NULL, SDL_MapRGB( m_screenSurface->format, 0xFF, 0xFF, 0xFF ) );
	//Update the surface
	SDL_UpdateWindowSurface( m_window );

	SDL_SetRelativeMouseMode((SDL_bool) true);

	LOG(INFO, "SDL initialized");

	m_glContext = SDL_GL_CreateContext(m_window);

	GLenum err = glewInit();
	if(GLEW_OK != err)
	{
		LOG(ERROR, "GLew initialization error: "<<glewGetErrorString(err));
		return false;
	}
	LOG(INFO,"GLEW initialized");

	glClearColor(0, 0, 0, 1); // black

	glViewport(0,0,m_width,m_height);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glCullFace(GL_FRONT);

	glEnable(GL_DEPTH_TEST);

	WindowResizedEventData windowResizedEventData;
	g_eventManager->addListenner(
		EventListenerDelegate::from_method
				<OpenGLRenderManager,&OpenGLRenderManager::onWindowResize>(this),
		windowResizedEventData.getType());

	EnginePausedEventData enginePausedEventData;
	g_eventManager->addListenner(
		EventListenerDelegate::from_method
				<OpenGLRenderManager,&OpenGLRenderManager::onEnginePause>(this),
		enginePausedEventData.getType());

	EngineResumedEventData engineResumedEventData;
	g_eventManager->addListenner(
		EventListenerDelegate::from_method
				<OpenGLRenderManager,&OpenGLRenderManager::onEngineResume>(this),
		engineResumedEventData.getType());

	return true;
}

bool OpenGLRenderManager::destroy()
{
	SDL_DestroyWindow( m_window );

	SDL_Quit();

	return true;
}

void printMatrix(float *camera, float *model)
{
	std::cout<<"camera========================================"<<std::endl;
	for(int i=0; i < 4; i++ )
	{
		for(int j=0; j<4; j++)
		{
			std::cout<<"\t\t"<<camera[i*4 + j];
		}
		std::cout<<std::endl;
	}
	std::cout<<"model========================================"<<std::endl;
	for(int i=0; i < 4; i++ )
	{
		for(int j=0; j<4; j++)
		{
			std::cout<<"\t\t"<<model[i*4 + j];
		}
		std::cout<<std::endl;
	}
}

void OpenGLRenderManager::preRender()
{
	//CLEAR
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glCullFace(GL_FRONT);

	glEnable(GL_DEPTH_TEST);

	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT   | GL_ENABLE_BIT  |
	             GL_TEXTURE_BIT      | GL_TRANSFORM_BIT | GL_VIEWPORT_BIT);
}

void OpenGLRenderManager::render(Scene& scene)
{
	scene.camera.setAspectRatio((float) m_width/m_height);

	Matrix4 projectionMatrix = scene.camera.getProjectionMatrix();

	Vec4 lightPos = Vec4(0,0,0,1) * scene.lights[0].getTransform()->getMatrix() * scene.camera.getTransform()->getMatrix();
	int objects = 0;
	for(auto itScene = scene.objects.begin();
			itScene != scene.objects.end(); itScene++)
	{
		if(itScene->getMeshRender())
		{
			objects++;
			std::shared_ptr<MeshRender> meshRender = itScene->getMeshRender();

			glUseProgram(meshRender->getShader()->getProgram());

			glBindVertexArray(meshRender->getVAO());

			int uniformModelView = meshRender->getShader()->getUniformLocation(UNIFORM_MODELVIEW_TRANSFORM);
			if(-1 != uniformModelView)
			{

				Matrix4 modelView = itScene->getTransform()->getMatrix() * scene.camera.getTransform()->getInverseMatrix();
				glUniformMatrix4fv(uniformModelView, 1, true, modelView.m_data);

				int uniformProjection = meshRender->getShader()->getUniformLocation(UNIFORM_PROJECTION_TRANSFORM);
				if(-1 != uniformProjection)
				{
					Matrix4 modelViewProjection = modelView * projectionMatrix;
					glUniformMatrix4fv(uniformProjection, 1, true, modelViewProjection.m_data);
				}
				else
				{
					LOG(WARN,"projection not found!");
				}

				int uniformNormal = meshRender->getShader()->getUniformLocation(UNIFORM_NORMAL_TRANSFORM);
				if(-1 != uniformNormal)
				{
					Matrix4 normalMatrix = itScene->getTransform()->getInverseMatrix() * scene.camera.getTransform()->getMatrix();
					glUniformMatrix4fv(uniformNormal, 1, false, normalMatrix.m_data);
				}
				else
				{
					LOG(WARN,"normal not found!");
				}

				int uniformLighPos = meshRender->getShader()->getUniformLocation(UNIFORM_LIGHT_POS);
				if(-1 != uniformLighPos)
				{
					float invW = lightPos.m_data[3];
					glUniform3f(uniformLighPos, lightPos.m_data[0] * invW , lightPos.m_data[1] * invW, lightPos.m_data[2] * invW);
				}
				else
				{
					LOG(WARN,"lightPos not found!");
				}
			}
			else
			{
				LOG(WARN,"modelView not found!");
			}

			glDrawElements(GL_TRIANGLES, meshRender->getMesh()->m_numVerticeIndices, GL_UNSIGNED_INT, 0); // 3 indices starting at 0 -> 1 triangle
		}
	}
}

void OpenGLRenderManager::postRender()
{
	//SWAP BUFFERS
	SDL_GL_SwapWindow(m_window);

	glPopAttrib();
}

void OpenGLRenderManager::onWindowResize(IEventDataPtr e)
{
	WindowResizedEventData* event = dynamic_cast<WindowResizedEventData*>(e.get());
	m_width = event->getWidth();
	m_height = event->getHeight();

	glViewport(0, 0, m_width, m_height);
}

void OpenGLRenderManager::onEnginePause(IEventDataPtr e)
{
	SDL_SetRelativeMouseMode((SDL_bool) false);	
}

void OpenGLRenderManager::onEngineResume(IEventDataPtr e)
{
	SDL_SetRelativeMouseMode((SDL_bool) true);		
}

