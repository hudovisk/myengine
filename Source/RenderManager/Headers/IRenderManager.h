/*
* IRenderManager.h
*
*  Created on: Apr 12, 2014
*      Author: hudo
*/

#pragma once

#include "GameObject.h"


class IRenderManager
{
public:

	virtual ~IRenderManager() { }

	virtual bool init(int width, int height) = 0;
	virtual bool destroy() = 0;

	virtual void render(Scene& scene) = 0;

	virtual void onWindowResize(IEventDataPtr event) = 0;

	virtual int getWidth() = 0;
	virtual int getHeight() = 0;
};
