/*
 * OpenGLRenderManager.h
 *
 *  Created on: Apr 12, 2014
 *      Author: hudo
 */

#pragma once

// #include "IRenderManager.h"
#include "Scene.h"
#include "IEventManager.h"

#include <SDL2/SDL.h>
#include <GL/glew.h>

class OpenGLRenderManager /*: public IRenderManager*/
{
public:
	bool init(int width, int height);
	bool destroy();

	void preRender();
	void render(Scene& scene);
	void postRender();

	void onWindowResize(IEventDataPtr event);
	void onEnginePause(IEventDataPtr event);
	void onEngineResume(IEventDataPtr event);

	int getWidth() { return m_width; }
	int getHeight() { return m_height; }
private:
	SDL_Window* m_window;
	SDL_Surface* m_screenSurface;
	SDL_GLContext m_glContext;

	int m_width, m_height;
};
