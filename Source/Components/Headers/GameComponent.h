
#pragma once

#include <memory>

class GameObject;

class GameComponent
{
public:

	virtual ~GameComponent() { }

	GameComponent(std::shared_ptr<GameObject> object) : m_object(object) { }
  
	virtual bool init() { return true; }
	virtual bool destroy() { return true; }
  
protected:
  std::shared_ptr<GameObject> m_object;
};
