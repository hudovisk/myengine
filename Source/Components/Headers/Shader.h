
#pragma once

#include <string>

typedef enum
{
	UNIFORM_MODELVIEW_TRANSFORM,
	UNIFORM_PROJECTION_TRANSFORM,
	UNIFORM_NORMAL_TRANSFORM,
	UNIFORM_LIGHT_POS,
} Uniforms;

class Shader
{
public:
	Shader()
		: m_vertexFilePath(std::string()), m_fragmentFilePath(std::string()), m_program(0),
		  m_initialised(false) { }
	~Shader() { destroy(); }

	bool init(std::string vertexFilePath, std::string fragmentFilePath);
	bool destroy();

	int getUniformLocation(Uniforms uniform);

	unsigned int getProgram() { return m_program; }

	bool isInitialised() { return m_initialised; }

private:

	std::string m_vertexFilePath;
	std::string m_fragmentFilePath;

	unsigned int m_program;
	bool m_initialised;


};
