
#pragma once

#include <string>

class Mesh
{
public:
	Mesh();
	Mesh(const Mesh& mesh);

	~Mesh();

	Mesh& operator=(const Mesh& mesh);

	bool load(std::string filePath);

	float *m_vertices;
	float *m_normals;
	unsigned int *m_verticeIndices;
	float *m_textureVertices;

	unsigned int m_numVertices;
	unsigned int m_numVerticeIndices;
	int m_vertexSize;
	int m_textureVerticeSize;

private:
	bool loadOBJ(std::string filePath);
};
