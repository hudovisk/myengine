#pragma once

#include "GameComponent.h"
#include "Shader.h"
#include "Mesh.h"

class MeshRender : public GameComponent
{
public:
	MeshRender(std::shared_ptr<GameObject> object, std::shared_ptr<Shader> shader, std::shared_ptr<Mesh> mesh) :
		GameComponent(object), m_vao(0), m_vbo(0), m_nbo(0),
		m_vboIndices(0), m_shader(shader), m_mesh(mesh)
	{

	}

	virtual ~MeshRender() { destroy(); }

	bool init();
	bool destroy();

	void setMesh(std::shared_ptr<Mesh> mesh) { m_mesh = mesh; }
	std::shared_ptr<Mesh> getMesh() { return m_mesh; }

	void setShader(std::shared_ptr<Shader> shader) { m_shader = shader; }
	std::shared_ptr<Shader> getShader() { return m_shader; }

	unsigned int getVAO() { return m_vao; }

private:
	unsigned int m_vao;
	unsigned int m_vbo; //vertex buffer object
	unsigned int m_nbo; //normal buffer object
	unsigned int m_vboIndices;
	std::shared_ptr<Shader> m_shader;
	std::shared_ptr<Mesh> m_mesh;
};


