/*
 * IController.h
 *
 *  Created on: Apr 13, 2014
 *      Author: hudo
 */

#pragma once

#include <list>

#include "GameComponent.h"
#include "InputTypes.h"

class IController : public GameComponent
{
public:
	IController(std::shared_ptr<GameObject> object) : GameComponent(object) { }
	virtual ~IController() { }
	virtual void update(float updateTime, std::list<InputEvent>& inputs) { }
};
