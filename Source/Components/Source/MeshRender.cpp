
#include "MeshRender.h"
#include <GL/glew.h>

bool MeshRender::init()
{
	if(!m_shader->isInitialised())
		return false;

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	//initialise vertex buffer
	glGenBuffers(1,&m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_mesh->m_numVertices * m_mesh->m_vertexSize * sizeof(float), m_mesh->m_vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
			0,                  	// attribute 0. No particular reason for 0, but must match the layout in the shader.
			m_mesh->m_vertexSize,	// size
			GL_FLOAT,           	// type
			GL_FALSE,           	// normalized?
			0,    			// stride
			(void*)0            	// array buffer offset
	);

	glGenBuffers(1,&m_nbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_nbo);
	glBufferData(GL_ARRAY_BUFFER, m_mesh->m_numVertices * m_mesh->m_vertexSize * sizeof(float), m_mesh->m_normals, GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
			1,                  	// attribute 1. No particular reason for 1, but must match the layout in the shader.
			m_mesh->m_vertexSize,	// size
			GL_FLOAT,           	// type
			GL_FALSE,           	// normalized?
			0,    			// stride
			(void*)0            	// array buffer offset
	);

	glGenBuffers(1, &m_vboIndices);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndices);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_mesh->m_numVerticeIndices * sizeof(unsigned int), m_mesh->m_verticeIndices, GL_STATIC_DRAW);

	glBindVertexArray(0);

	return true;
}

bool MeshRender::destroy()
{
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_vboIndices);
	glDeleteVertexArrays(1, &m_vao);

	return true;
}


