set(components_source_files MeshRender.cpp Shader.cpp Mesh.cpp)
set(components_headers_files ../Headers/GameComponent.h ../Headers/MeshRender.h ../Headers/Mesh.h ../Headers/Shader.h ../Headers/IController.h)

find_package(OpenGL REQUIRED)
include_directories( ${OPENGL_INCLUDE_DIRS} )

add_library(render_component ${components_source_files} ${components_headers_files})

message("GL = ${OPENGL_LIBRARIES}")

#target_link_libraries(render_component my_debug ${OPENGL_LIBRARIES} GLEW )
target_link_libraries(render_component my_debug GLU GL GLEW )