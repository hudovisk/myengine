
#include "Mesh.h"
#include "Logger.h"
#include "Vec3.h"

#include <cstdio>
#include <vector>
#include <cstring>

#include <cstdlib>

Mesh::Mesh()
	: m_vertices(nullptr), m_normals(nullptr), m_verticeIndices(nullptr),
	  m_textureVertices(nullptr), m_numVertices(0), m_numVerticeIndices(0),
	  m_vertexSize(3), m_textureVerticeSize(2)
{

}

Mesh::Mesh(const Mesh& mesh)
	: m_vertices(nullptr), m_normals(nullptr), m_verticeIndices(nullptr),
	  m_textureVertices(nullptr), m_numVertices(0), m_numVerticeIndices(0),
	  m_vertexSize(3), m_textureVerticeSize(0)
{
	if(mesh.m_vertices)
	{
		m_vertices = new float [mesh.m_vertexSize * mesh.m_numVertices];
		memcpy(m_vertices,mesh.m_vertices, mesh.m_vertexSize * mesh.m_numVertices * sizeof(float));

		m_vertexSize = mesh.m_vertexSize;
		m_numVertices = mesh.m_numVertices;

		if(mesh.m_normals)
		{
			m_normals = new float [mesh.m_vertexSize * mesh.m_numVertices];
			memcpy(m_normals,mesh.m_normals, mesh.m_vertexSize * mesh.m_numVertices * sizeof(float));
		}

		if(mesh.m_verticeIndices)
		{
			m_verticeIndices = new unsigned int [mesh.m_numVerticeIndices];
			memcpy(m_verticeIndices, mesh.m_verticeIndices, mesh.m_numVerticeIndices * sizeof(unsigned int));

			m_numVerticeIndices = mesh.m_numVerticeIndices;
		}

		if(mesh.m_textureVertices)
		{
			m_textureVertices = new float[mesh.m_numVertices * mesh.m_textureVerticeSize];
			memcpy(m_textureVertices, mesh.m_textureVertices, mesh.m_numVertices * mesh.m_textureVerticeSize * sizeof(float) );

			m_textureVerticeSize = mesh.m_textureVerticeSize;
		}
	}
}

Mesh::~Mesh()
{
	delete [] m_vertices;
	delete [] m_verticeIndices;
	delete [] m_normals;
	delete [] m_textureVertices;
}

bool Mesh::load(std::string filePath)
{
	int pos = filePath.find_last_of('.');

	if(filePath.substr(pos, pos - filePath.length()) == ".obj")
		return loadOBJ(filePath);

	LOG(ERROR, "File type of mesh: "<<filePath<<" not supported");

	return false;
}

bool Mesh::loadOBJ(std::string filePath)
{
	FILE* file = fopen(filePath.c_str(), "r");
	if(file == nullptr)
	{
		LOG(ERROR, "Could not open mesh file: "<<filePath);
		return false;
	}

	std::vector<float> vertices;
	std::vector<unsigned int> verticesIndices;

//	std::vector<unsigned int> normalIndices;

	char line[256];
	while(fgets(line, 256, file))
	{
		if(line[0] == '#') //COMMENTARY
			continue;

		if(line[0] == 'v') //VERTEX
		{
			if(line[1] == ' ') //VERTEX POSITION
			{
				float x,y,z;

				sscanf(line, "v %f %f %f\n", &x, &y, &z);

				vertices.push_back(x);
				vertices.push_back(y);
				vertices.push_back(z);
			}
//			else if(line[1] == 'n') //VERTEX NORMAL
//			{
//				float x,y,z;
//
//				sscanf(line, "%f %f %f\n", &x, &y, &z);
//
//				normal.push_back(x);
//				normal.push_back(y);
//				normal.push_back(z);
//			}
		}
		else if(line[0] == 'f') //FACE
		{
			unsigned int idxVertice1, idxVertice2, idxVertice3;
			unsigned int idxNormal1, idxNormal2, idxNormal3;

//			sscanf(line, "f %d/[%*d]/%d %d/[%*d]/%d %d/[%*d]/%d\n",&idxVertice1, &idxNormal1, &idxVertice2, &idxNormal2,
//													&idxVertice3, &idxNormal3);

			sscanf(line, "f %d//%d %d//%d %d//%d\n",&idxVertice1, &idxNormal1, &idxVertice2, &idxNormal2,
																&idxVertice3, &idxNormal3);

			verticesIndices.push_back(idxVertice1 - 1);
			verticesIndices.push_back(idxVertice2 - 1);
			verticesIndices.push_back(idxVertice3 - 1);

//			normalIndices.push_back(idxNormal1 - 1);
//			normalIndices.push_back(idxNormal2 - 1);
//			normalIndices.push_back(idxNormal3 - 1);

		}
	}

	std::vector<float> normals(vertices.size(), 0);
	for(unsigned int i = 0; i < verticesIndices.size(); i+=3)
	{
		unsigned int ia = verticesIndices[i] * 3;
		unsigned int ib = verticesIndices[i+1] * 3;
		unsigned int ic = verticesIndices[i+2] * 3;

		Vec3 a(vertices[ia], vertices[ia+1], vertices[ia+2]);
		Vec3 b(vertices[ib], vertices[ib+1], vertices[ib+2]);
		Vec3 c(vertices[ic], vertices[ic+1], vertices[ic+2]);

		Vec3 normal = (b - a).crossProduct(c - a);
		normal.normalize();

		normals[ia] = normals[ib] = normals[ic] = normal.m_data[0];
		normals[ia+1] = normals[ib+1] = normals[ic+1] = normal.m_data[1];
		normals[ia+2] = normals[ib+2] = normals[ic+2] = normal.m_data[2];
	}

	m_vertexSize = 3;

	m_numVertices = vertices.size() / m_vertexSize;
	m_vertices = new float[vertices.size()];
	memcpy(m_vertices, vertices.data(), vertices.size() * sizeof(float));

	m_normals = new float[normals.size()];
	memcpy(m_normals, normals.data(), normals.size() * sizeof(float));

	m_numVerticeIndices = verticesIndices.size();
	m_verticeIndices = new unsigned int[verticesIndices.size()];
	memcpy(m_verticeIndices, verticesIndices.data(), m_numVerticeIndices * sizeof(unsigned int));

	fclose(file);

	return true;
}
