#include <iostream>
#include <string>
#include <functional>

#include "EngineImpl.h"
#include "../Game/Core/Headers/TestGame.h"

int main(int argc, char* argv[])
{
	IGame* game = new TestGame();
  
	IEngine* engine = new EngineImpl(game);
	if(!engine->init())
	{
		std::cout<<"Erro na inicialização da engine"<<std::endl;
		return 1;
	}

	engine->mainLoop();

	return 0;
}
