# Install script for directory: /home/hudo/Projects/MyEngine/ext/cegui-0.8.4

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/CEGUI-0.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/CEGUI-0-LUA.pc")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/cmake_install.cmake")
  include("/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/RendererModules/cmake_install.cmake")
  include("/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/XMLParserModules/cmake_install.cmake")
  include("/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/ImageCodecModules/cmake_install.cmake")
  include("/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/WindowRendererSets/cmake_install.cmake")
  include("/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/ScriptModules/cmake_install.cmake")
  include("/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/CommonDialogs/cmake_install.cmake")
  include("/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/datafiles/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

file(WRITE "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/${CMAKE_INSTALL_MANIFEST}" "")
foreach(file ${CMAKE_INSTALL_MANIFEST_FILES})
  file(APPEND "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
endforeach()
