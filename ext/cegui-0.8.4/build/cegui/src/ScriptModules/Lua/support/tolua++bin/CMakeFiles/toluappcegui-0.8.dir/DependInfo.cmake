# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/ScriptModules/Lua/support/tolua++bin/tolua.c" "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/ScriptModules/Lua/support/tolua++bin/CMakeFiles/toluappcegui-0.8.dir/tolua.c.o"
  "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/ScriptModules/Lua/support/tolua++bin/toluabind.c" "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/ScriptModules/Lua/support/tolua++bin/CMakeFiles/toluappcegui-0.8.dir/toluabind.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "cegui/include"
  "../cegui/include"
  "/usr/include/lua5.1"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
