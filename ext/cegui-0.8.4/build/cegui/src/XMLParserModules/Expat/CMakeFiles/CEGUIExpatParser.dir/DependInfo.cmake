# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/XMLParserModules/Expat/XMLParser.cpp" "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/XMLParserModules/Expat/CMakeFiles/CEGUIExpatParser.dir/XMLParser.cpp.o"
  "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/XMLParserModules/Expat/XMLParserModule.cpp" "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/XMLParserModules/Expat/CMakeFiles/CEGUIExpatParser.dir/XMLParserModule.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/CMakeFiles/CEGUIBase-0.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "cegui/include"
  "../cegui/include"
  "../cegui/include/XMLParserModules/Expat"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
