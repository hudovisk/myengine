# Install script for directory: /home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libCEGUIBase-0.so.2.3.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libCEGUIBase-0.so.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libCEGUIBase-0.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/lib/libCEGUIBase-0.so.2.3.0"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/lib/libCEGUIBase-0.so.2"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/lib/libCEGUIBase-0.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libCEGUIBase-0.so.2.3.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libCEGUIBase-0.so.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libCEGUIBase-0.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cegui-0/CEGUI" TYPE FILE FILES
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/EventSet.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/DataContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MemoryOgreAllocator.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MinizipResourceProvider.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/String.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/PropertyHelper.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/ChainedXMLHandler.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Base.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/CoordConverter.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FactoryRegisterer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderedString.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/ColourRect.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FreeFunctionSlot.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MinibidiVisualMapping.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FribidiVisualMapping.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/PropertySet.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderingSurface.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/WindowRenderer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TplWRFactoryRegisterer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/IconvStringTranscoder.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/JustifiedRenderedString.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderEffectFactory.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TplInterpolators.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Image.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/ResourceProvider.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderedStringParser.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Font_xmlHandler.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RightAlignedRenderedString.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/IteratorBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/WindowManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/LinkedEvent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/DefaultResourceProvider.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/WindowFactoryManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Singleton.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderedStringTextComponent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/SystemKeys.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Rect.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderedStringImageComponent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FunctorPointerSlot.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderTarget.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderedStringComponent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Texture.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/WindowFactory.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FontGlyph.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TplWindowFactory.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FontManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/BoundSlot.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Font.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderEffect.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/BidiVisualMapping.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderingContext.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/ImageCodec.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/CentredRenderedString.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/SlotFunctorBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderEffectManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TextureTarget.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Config_xmlHandler.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Colour.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Event.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/InjectedInputReceiver.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/XMLParser.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/SimpleTimer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Win32StringTranscoder.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Affector.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Animation.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/ImageFactory.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/UDim.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MemoryAllocatedObject.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/PixmapFont.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/ForwardRefs.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/DynamicModule.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/GUIContext.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Quaternion.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/ImageManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Scheme_xmlHandler.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Logger.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderingWindow.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RefCounted.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/AnimationInstance.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FunctorReferenceBinder.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TypedProperty.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/KeyFrame.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/NamedElement.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/PCRERegexMatcher.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/GUILayout_xmlHandler.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TplProperty.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FormattedRenderedString.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/SchemeManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Exceptions.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/LeftAlignedRenderedString.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/SubscriberSlot.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/EventArgs.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/XMLSerializer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderQueue.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/DefaultRenderedStringParser.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/StringTranscoder.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Interpolator.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Size.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Clipboard.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FunctorCopySlot.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/ScriptModule.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MemorySTLWrapper.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/XMLHandler.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/CompositeResourceProvider.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderedStringWordWrapper.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Vector.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TplWindowProperty.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Animation_xmlHandler.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/WindowRendererManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MemberFunctionSlot.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TplWindowRendererFactory.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/NamedXMLResourceManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/InputEvent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/GeometryBuffer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MouseCursor.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Element.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TplWindowRendererProperty.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/System.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/BasicImage.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/BasicRenderedStringParser.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Vertex.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RenderedStringWidgetComponent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FunctorReferenceSlot.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/AnimationManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Scheme.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FactoryModule.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Renderer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TextUtils.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/XMLAttributes.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Property.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/DefaultLogger.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/RegexMatcher.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MemoryStdAllocator.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/FreeTypeFont.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/MemoryAllocation.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/TplWindowFactoryRegisterer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/CEGUI.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/Window.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/GlobalEventSet.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/../include/CEGUI/Config.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/../include/CEGUI/ModuleConfig.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/build/cegui/src/../include/CEGUI/Version.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cegui-0/CEGUI/widgets" TYPE FILE FILES
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/DefaultWindow.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ClippedContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/GroupBox.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Spinner.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Combobox.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/VerticalLayoutContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ListboxTextItem.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ComboDropList.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/RadioButton.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Listbox.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ToggleButton.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Slider.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/LayoutCell.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Tooltip.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Scrollbar.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/All.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ItemListBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ItemListbox.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/TabControl.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/SequentialLayoutContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Thumb.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/LayoutContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ListboxItem.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ScrolledContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/GridLayoutContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/HorizontalLayoutContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ItemEntry.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Titlebar.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/MultiColumnList.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ProgressBar.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/DragContainer.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/MultiLineEditbox.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/FrameWindow.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ScrollablePane.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/TabButton.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/PopupMenu.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Editbox.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ButtonBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Tree.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ScrolledItemListBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/PushButton.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/Menubar.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ListHeader.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/ListHeaderSegment.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/TreeItem.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/MenuBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/widgets/MenuItem.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cegui-0/CEGUI/falagard" TYPE FILE FILES
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/Enums.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/SectionSpecification.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/Dimensions.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/FalagardPropertyBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/NamedDefinitionCollator.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/PropertyDefinitionBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/PropertyDefinition.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/StateImagery.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/PropertyLinkDefinition.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/WidgetComponent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/FormattingSetting.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/NamedArea.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/WidgetLookManager.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/XMLHandler.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/WidgetLookFeel.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/FrameComponent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/PropertyInitialiser.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/ImageryComponent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/TextComponent.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/XMLEnumHelper.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/LayerSpecification.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/ComponentBase.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/EventLinkDefinition.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/ImagerySection.h"
    "/home/hudo/Projects/MyEngine/ext/cegui-0.8.4/cegui/src/../include/CEGUI/falagard/EventAction.h"
    )
endif()

