
#pragma once

#include "IController.h"
#include "Vec3.h"
#include "InputContext.h"

class PlayerController : public IController
{
public:
  PlayerController(std::shared_ptr<GameObject> object) : IController(object) { }
  ~PlayerController() { }
  
  bool init();
  void update(float updateTime, std::list<InputEvent>& inputs);
  
  float m_speed;
  Vec3 m_pos;
  Vec3 m_movingDirection;
  int m_lastMousePos[2];
  
  InputContext m_inputContext;
};
