#pragma once

#include "IController.h"
#include "InputContext.h"
#include "Vec3.h"

class CameraController: public IController {
public:
	CameraController(std::shared_ptr<GameObject> object) :
			IController(object), m_speed(0), m_pos(Vec3(0,0,0)),
			m_movingDirection(Vec3(0,0,0))
	{
		m_lastMousePos[0] = 0;
		m_lastMousePos[1] = 0;
	}
	~CameraController() {
	}

	bool init();
	void update(float updateTime, std::list<InputEvent>& inputs);

	InputContext m_inputContext;
	float m_speed;
	Vec3 m_pos;
	Vec3 m_movingDirection;

	int m_lastMousePos[2];
};
