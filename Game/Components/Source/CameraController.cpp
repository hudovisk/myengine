#include "CameraController.h"
#include <GameObject.h>
#include "OpenGLRenderManager.h"
#include "Vec4.h"
#include <iostream>

extern OpenGLRenderManager* g_renderManager;

enum {
	START_MOVE_FORWARD = 0x01,
	STOP_MOVE_FORWARD_BACKWARD = 0x02,
	LOOK_MOVEMENT = 0X03,
	START_MOVE_LEFT = 0x04,
	START_MOVE_RIGHT = 0x05,
	STOP_MOVE_LEFT_RIGHT = 0x06,
	START_MOVE_BACKWARD = 0x07,
};

bool CameraController::init() {
//	m_object->getTransform().scale(Vec3(1,1,-1));
	m_lastMousePos[0] = g_renderManager->getWidth() / 2;
	m_lastMousePos[1] = g_renderManager->getHeight() / 2;
	return m_inputContext.init("res/mappings/camera.xml");
}

// int degreeX = 0;
// int degreeY = 0;
void CameraController::update(float updateTime, std::list<InputEvent>& inputs) {
// 	std::vector<InputMessage> inputMessages = m_inputContext.parse(inputs);

// 	float rotation_speed = 1;
// 	float movmentSpeed = .1;
// 	m_movingDirection = Vec3(0,0,0);
// 	for (auto itMessage = inputMessages.begin();
// 			itMessage != inputMessages.end(); itMessage++) {
// 		switch (itMessage->id) {
// 		case START_MOVE_FORWARD:
// 			m_speed = movmentSpeed;
// 			m_movingDirection.m_data[2] = -1;
// 			break;
// 		case START_MOVE_BACKWARD:
// 			m_speed = movmentSpeed;
// 			m_movingDirection.m_data[2] = 1;
// 			break;
// 		case STOP_MOVE_FORWARD_BACKWARD:
// 			m_speed = 0;
// 			m_movingDirection.m_data[2] = 0;
// 			break;

// 		case START_MOVE_LEFT:
// 			m_speed = movmentSpeed;
// 			m_movingDirection.m_data[0] = -1;
// 			break;
// 		case START_MOVE_RIGHT:
// 			m_speed = movmentSpeed;
// 			m_movingDirection.m_data[0] = 1;
// 			break;
// 		case STOP_MOVE_LEFT_RIGHT:
// 			m_speed = 0;
// 			m_movingDirection.m_data[0] = 0;
// 			break;
// 		case LOOK_MOVEMENT:
// 			// degreeX = (itMessage->pos[0] - g_renderManager->getWidth()/2)/(g_renderManager->getWidth()/2.0) * 90 /** rotation_speed*/;
// 			// degreeY = (itMessage->pos[1] - g_renderManager->getHeight()/2)/(g_renderManager->getHeight()/2.0) * 90/* * rotation_speed*/;
// 			degreeX += itMessage->pos[0] - m_lastMousePos[0];
// 			degreeY += itMessage->pos[1] - m_lastMousePos[1];
// 			m_lastMousePos[0] = itMessage->pos[0];
// 			m_lastMousePos[1] = itMessage->pos[1];

// 			break;
// 		}
// 	}

// 	if(degreeX || degreeY)
// 	{
// 		m_object->getTransform()->setRotationInDegree(
// 						degreeX%360, Vec3(0, 1, 0));
// 		m_object->getTransform()->rotateInDegree(
// 						degreeY%360, Vec3(1, 0, 0));

// 		// std::cout<<"degreeX: "<<degreeX<<" degreeY: "<<degreeY<<std::endl;
// 	}

// 	if (m_movingDirection.m_data[0] != 0 || m_movingDirection.m_data[1] != 0 || m_movingDirection.m_data[2] != 0)
// 	{
// 		Vec4 mov4(m_movingDirection.m_data[0], m_movingDirection.m_data[1], m_movingDirection.m_data[2], 0);
// 		mov4 = mov4 * m_object->getTransform()->getMatrix();

// 		m_movingDirection = Vec3(mov4.m_data[0], mov4.m_data[1], mov4.m_data[2]);

// 		m_movingDirection.normalize();
// 		m_pos = m_pos + m_movingDirection * m_speed;
// 		m_object->getTransform()->translate(m_pos);
// 	}
}

