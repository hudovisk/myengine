#include "PlayerController.h"

#include <iostream>
#include <cstdlib>
#include "GameObject.h"
#include "OpenGLRenderManager.h"


enum {
	START_MOVE_FORWARD = 0x01,
	STOP_MOVE_FORWARD_BACKWARD = 0x02,
	LOOK_MOVEMENT = 0X03,
	START_MOVE_LEFT = 0x04,
	START_MOVE_RIGHT = 0x05,
	STOP_MOVE_LEFT_RIGHT = 0x06,
	START_MOVE_BACKWARD = 0x07,
};

bool PlayerController::init() {
	m_lastMousePos[0] = -1;
	m_lastMousePos[1] = -1;
	return m_inputContext.init("res/mappings/playerInputMap.xml");
}

float yaw = 0;
float pitch = 0;
void PlayerController::update(float updateTime, std::list<InputEvent>& inputs) {
	std::vector<InputMessage> inputMessages = m_inputContext.parse(inputs);

	float rotation_speed = .001;
	float movmentSpeed = .1;
	m_movingDirection = Vec3(0,0,0);
	for (auto itMessage = inputMessages.begin();
			itMessage != inputMessages.end(); itMessage++) {
		switch (itMessage->id) {
		case START_MOVE_FORWARD:
			m_speed = movmentSpeed;
			m_movingDirection.m_data[2] = -1;
			break;
		case START_MOVE_BACKWARD:
			m_speed = movmentSpeed;
			m_movingDirection.m_data[2] = 1;
			break;
		case STOP_MOVE_FORWARD_BACKWARD:
			m_speed = 0;
			m_movingDirection.m_data[2] = 0;
			break;

		case START_MOVE_LEFT:
			m_speed = movmentSpeed;
			m_movingDirection.m_data[0] = -1;
			break;
		case START_MOVE_RIGHT:
			m_speed = movmentSpeed;
			m_movingDirection.m_data[0] = 1;
			break;
		case STOP_MOVE_LEFT_RIGHT:
			m_speed = 0;
			m_movingDirection.m_data[0] = 0;
			break;
		case LOOK_MOVEMENT:
			yaw = (int) ( (yaw + itMessage->pos[0] * rotation_speed) * 1000) % 3141 * 0.001;
			float tempPitch = pitch + itMessage->pos[1] * rotation_speed;
			if(fabs(tempPitch) < 0.785398163397448)
				pitch = tempPitch;

			Vec3 globalPitchAxis(1,0,0);
			Quaternion qPitch(cos(pitch), globalPitchAxis * sin(pitch));

			Vec3 localYawAxis = (qPitch * Quaternion(0,Vec3(0,1,0)) * qPitch.getInverse()).m_axis;
			Quaternion qYaw(cos(yaw), localYawAxis * sin(yaw));

			m_object->getTransform()->setRotation(qYaw * qPitch);
	
			// std::cout<<"Degree yaw: "<<yaw * 114.591559026165 <<" Degree pitch: "<<pitch * 114.591559026165 <<std::endl;
			break;
		}
	}

	if (m_movingDirection.m_data[0] != 0 || m_movingDirection.m_data[1] != 0 || m_movingDirection.m_data[2] != 0)
	{
		Vec4 mov4(m_movingDirection.m_data[0], m_movingDirection.m_data[1], m_movingDirection.m_data[2], 0);
		mov4 = mov4 * m_object->getTransform()->getMatrix();

		m_movingDirection = Vec3(mov4.m_data[0], mov4.m_data[1], mov4.m_data[2]);

		m_movingDirection.normalize();
		m_pos = m_pos + m_movingDirection * m_speed;
		m_object->getTransform()->translate(m_pos);
	}
}
