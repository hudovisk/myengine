#pragma once

#include "IGame.h"
#include "GameObject.h"
#include "Scene.h"


class TestGame: public IGame {
public:

	TestGame() { }
	~TestGame() { destroy(); }

	bool init();
	bool destroy();

	void update(float updateTime, std::list<InputEvent>& inputs);

	void render();

private:
	GameObject m_player;
	GameObject m_object;
	Camera m_camera;
	Scene m_scene;
};
