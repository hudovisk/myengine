/*
 * GameObjectFactory.h
 *
 *  Created on: Apr 13, 2014
 *      Author: hudo
 */

#pragma once

#include "GameObject.h"

class GameObjectFactory
{
public:
	static bool createObject(GameObject* object);
	static bool createPlayer(GameObject* player);
	static bool createCamera(Camera* camera);
};
