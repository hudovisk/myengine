#include "TestGame.h"
#include "GameObjectFactory.h"
#include "OpenGLRenderManager.h"

extern OpenGLRenderManager* g_renderManager;

bool TestGame::init() {

	if(!GameObjectFactory::createPlayer(&m_player))
		return false;
	if(!GameObjectFactory::createCamera(&(m_scene.camera)))
		return false;
	if(!GameObjectFactory::createObject(&m_object))
		return false;

	m_player.getTransform()->translate(Vec3(0,0,0));
	
	m_object.getTransform()->translate(Vec3(0,0,0));

	std::shared_ptr<Transform> camera_transform(new Transform());
	camera_transform->translate(Vec3(0,0,5));
	// camera_transform->rotateInDegree(45,Vec3(1,0,0));
	m_scene.camera.setTransform(/*camera_transform*/m_player.getTransform());
//	m_player.getTransform().scale(Vec3(.01,.01,.01));

	m_scene.objects.push_back(m_player);
	m_scene.objects.push_back(m_object);

	Light light(10);
	std::shared_ptr<Transform> transform(new Transform());
	light.setTransform(transform);
	light.getTransform()->translate(Vec3(50,0,0));
	m_scene.lights.push_back(light);

	return true;
}

bool TestGame::destroy() {
	m_player.destroy();
	//m_camera.destroy();

	m_scene.objects.clear();

	return true;
}

void TestGame::update(float updateTime, std::list<InputEvent>& inputs) {
	m_player.getController()->update(updateTime, inputs);
	//m_scene.camera.getController()->update(updateTime,inputs);
}

void TestGame::render() {
	g_renderManager->render(m_scene);
}
