/*
 * GameObjectFactory.cpp
 *
 *  Created on: Apr 13, 2014
 *      Author: hudo
 */

#include "GameObjectFactory.h"
#include <PlayerController.h>
#include <CameraController.h>

#include "Mesh.h"
#include "Logger.h"

bool GameObjectFactory::createObject(GameObject* object)
{
	std::shared_ptr<Shader> shader(new Shader);
	if(!shader->init("res/shaders/vertex.glsl","res/shaders/fragment.glsl"))
	{
		LOG(WARN, "Shaders not initialised");
		return false;
	}

	std::shared_ptr<Mesh> mesh(new Mesh);
	mesh->load("res/models/teapot.obj");

	std::shared_ptr<MeshRender> render(new MeshRender(std::shared_ptr<GameObject>(object), shader, mesh));
	if(!render->init())
	{
		LOG(ERROR, "RenderMesh not initialised");
		return false;
	}

	std::shared_ptr<Transform> transform(new Transform);

	object->setTransform(transform);
	object->setMeshRender(render);
	
	return true;
}

bool GameObjectFactory::createCamera(Camera* camera)
{
	std::shared_ptr<IController> controller(new CameraController(std::shared_ptr<Camera>(camera)));
	if(!controller->init())
	{
		LOG(ERROR, "CameraController not initialised");
		return false;
	}
	camera->setController(controller);

	camera->setFov(90);
	camera->setAspectRatio(1);
	camera->setNearDistance(.1);
	camera->setFarDistance(10);

	return true;
}

bool GameObjectFactory::createPlayer(GameObject* player)
{
	std::shared_ptr<Shader> shader(new Shader);
	if(!shader->init("res/shaders/vertex.glsl","res/shaders/fragment.glsl"))
	{
		LOG(WARN, "Shaders not initialised");
		return false;
	}

	std::shared_ptr<Mesh> mesh(new Mesh);
	mesh->load("res/models/monkey.obj");

	std::shared_ptr<MeshRender> render(new MeshRender(std::shared_ptr<GameObject>(player), shader, mesh));
	if(!render->init())
	{
		LOG(ERROR, "RenderMesh not initialised");
		return false;
	}

	std::shared_ptr<IController> controller(new PlayerController(std::shared_ptr<GameObject>(player)));
	if(!controller->init())
	{
		LOG(ERROR, "PlayerController not initialised");
		return false;
	}

	std::shared_ptr<Transform> transform(new Transform);

	player->setTransform(transform);
	player->setMeshRender(render);
	player->setController(controller);
	
	return true;
}


