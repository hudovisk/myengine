# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is my attempt to create a game engine.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

How to compile:

    git clone https://hudovisk@bitbucket.org/hudovisk/myengine.git
    cd myengine/
    mkdir build
    cd build/
    cmake ..
    make install //install target just copy the bin to the bin folder inside the project
    cd ..
    cp bin/Linux/game_x64 ./
    ./game_x64

tip: for developing with eclipse 4.3

    cmake ../MyEngine -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_ECLIPSE_VERSION=4.3 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_COMPILER_ARG1=-std=c++11

### TODO: ###
* Fix the rotations!
* Transformations Heritage!
* ...
